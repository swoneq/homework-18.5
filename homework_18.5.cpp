 #include <iostream>

class Stack
{
public:
	int size;
	int last_index;
	int* array;

	Stack()
	{
		size = 10;
		array = new int[size];
		last_index = -1;
	}
	Stack(int array_size)
	{
		size = array_size;
		array = new int[size];
		last_index = -1;
	}
	void push(int new_element)
	{
		if (last_index < size)
		{
			last_index++;
			array[last_index] = new_element;
		}
		else
		{
			int* temp_array = new int[size * 2];
			for (int i = 0; i < size; i++)
			{
				temp_array[i] = array[i];
			}
			delete[] array;
			array = temp_array;
			size = size * 2;
		}
	}
		
	int pop()
	{
		return array[last_index--];
	}

	void print()
	{
		for (int i = 0; i <= last_index; i++)
		{
			std::cout << array[i] << std::endl;
		}
	}
	
};

int main()
{
	Stack data_stack;
	data_stack.push(5);
	data_stack.print();
	data_stack.pop();
	data_stack.push(5);
	data_stack.print();
	return 0;
}

